// alert('hello')

// in this line of code we are getting the document in the HTML element with the id txt-first-name

// console.log(document);

const txtFirstName = document.querySelector("#txt-first-name");

// When using query selector, selectors should be included. 

// alternative way of targeting an element
// document.getElementById("txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

	


const spanFullName = document.querySelector("#span-full-name")



txtFirstName.addEventListener('keyup', (event) => {

	// Inner html allow us to call the tag and change it dynamically.

	spanFullName.innerHTML = txtFirstName.value

});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
});


